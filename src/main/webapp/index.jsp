<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="java.text.DateFormat, java.text.SimpleDateFormat, java.util.Date, java.util.TimeZone"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>JSP Activity</title>
</head>
<body>
	
	
	<h1>Our Date and Time now is...</h1>

	<%
	
		Date dateTime = new Date();
	
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String manila = df.format(dateTime);
		
		df.setTimeZone(TimeZone.getTimeZone("Asia/Tokyo"));
	    String japan = df.format(dateTime);
	    
	    df.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
	    String germany = df.format(dateTime);
	%>
	
	<ul>
	
			<li>Manila: <%= manila %></li>
			<li>Japan: <%= japan %></li>
			<li>Germany: <%= germany %></li>
		
	</ul>
	
	<%!
		private int initVar=3;
		private int serviceVar=3;
		private int destroyVar=3;
	%>
	
	
	<%!
		public void jspInit(){
	    	initVar--;
	    	System.out.println("jspInit(): init"+initVar);
	  		}
	
	  	public void jspDestroy(){
	    	destroyVar--;
	    	destroyVar = destroyVar + initVar;
	    	System.out.println("jspDestroy(): destroy"+destroyVar);
	  	}
	%>
	
	<%
		serviceVar--;
	
	  	System.out.println("_jspService(): service"+serviceVar);
	  	
	  	String content1="content1 : "+initVar;
	  	String content2="content2 : "+serviceVar;
	  	String content3="content3 : "+destroyVar;
	%>
	
	<h1>JSP</h1>
	<p><%= content1 %></p>
	<p><%= content2 %></p>
	<p><%= content3 %></p>

</body>
</html>